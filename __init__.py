from st3m.application import Application, ApplicationContext
import st3m.run
import leds

class Hypnotoad(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._time = 0
        self._frame = 0
        self._led = 0
        self._brightness = 255

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        if self._frame == 0:
            ctx.image("/flash/sys/apps/hypnotoad/frame0.png", -120, -90, 240, 180)
        else:
            ctx.image("/flash/sys/apps/hypnotoad/frame1.png", -120, -90, 240, 180)

        # blinkenlights
        # sometimes glitchy. why? shrug. good enough.
        leds.set_all_rgb(0, 0, 0)
        leds.set_rgb(self._led, self._brightness, self._brightness, self._brightness)
        leds.set_rgb(self._led+8, self._brightness, self._brightness, self._brightness)
        leds.set_rgb(self._led+16, self._brightness, self._brightness, self._brightness)
        leds.set_rgb(self._led+24, self._brightness, self._brightness, self._brightness)
        leds.set_rgb(self._led+32, self._brightness, self._brightness, self._brightness)
        leds.update()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms) # Let Application do its thing

        # slowly adjust brightness with app buttons
        direction = ins.buttons.app
        if self._brightness > 0 and direction == ins.buttons.PRESSED_LEFT:
            self._brightness -= 1
        elif self._brightness < 255 and direction == ins.buttons.PRESSED_RIGHT:
            self._brightness += 1

        # very roughly switch frames every 100ms
        self._time += delta_ms
        while self._time >= 100:
            #print(self._time)
            self._frame = (self._frame + 1) % 2
            self._led = (self._led + 1) % 8
            self._time -= 100
        
if __name__ == '__main__':
    st3m.run.run_view(Hypnotoad(ApplicationContext()))
